package al1.esgi.app.pa.esgi2019.home;

import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.content.Context;
import com.android.volley.Response;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.json.JSONArray;

import java.util.ArrayList;

public class DiscoveryUtils {
    Context ctx;
    Utils utils;

    public DiscoveryUtils(Context ctx) {
        this.ctx = ctx;
        this.utils = new Utils(ctx);
    }

    public void getRecentTrips(final AsyncResponse<ArrayList<Trip>> listener) {
        utils.callAPIArray("GET", "/voyages?_sort=created_at:DESC", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                ArrayList<Trip> trips = tripJSONResponseToArray(jsonArray);
                listener.setResponseData(trips).run();
            }
        });
    }

    public void getUserTrips(final AsyncResponse<ArrayList<Trip>> listener, int userId) {
        utils.callAPIArray("GET", "/voyages?user=" + userId + "&_sort=created_at:DESC", new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray jsonArray) {
                ArrayList<Trip> trips = tripJSONResponseToArray(jsonArray);
                listener.setResponseData(trips).run();
            }
        });
    }

    public ArrayList<Trip> tripJSONResponseToArray(JSONArray response) {
        JsonArray jsonArray = new JsonParser().parse(response.toString()).getAsJsonArray();
        ArrayList<Trip> trips = new ArrayList<>();
        for (JsonElement element : jsonArray) {
            trips.add(Trip.get(element.getAsJsonObject()));
        }
        return trips;
    }
}
