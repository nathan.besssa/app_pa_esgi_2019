package al1.esgi.app.pa.esgi2019.trip;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.model.Interest;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.content.Context;
import android.content.DialogInterface;
import android.text.InputType;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import androidx.appcompat.app.AlertDialog;
import com.android.volley.Response;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class TripInterestUtils {
    private List<Interest> interests;
    private int tripId = -1;
    private Context ctx;
    private ChipGroup interestChipGroup;
    private Utils utils;
    private boolean isEditMode;

    public TripInterestUtils(Context ctx) {
        this.ctx = ctx;
        this.interests = new ArrayList<>();
        this.utils = new Utils(ctx);
    }

    public TripInterestUtils(Context ctx, List<Interest> interests, int tripId) {
        this.ctx = ctx;
        this.interests = interests;
        this.tripId = tripId;
        this.utils = new Utils(ctx);
    }

    public void setViewGroup(ChipGroup interestChipGroup, boolean isEditMode) {
        this.isEditMode = isEditMode;
        this.interestChipGroup = interestChipGroup;
    }

    public void updateView() {
        interestChipGroup.removeAllViews();
        int paddingDp = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 10,
                ctx.getResources().getDisplayMetrics()
        );
        for (int i = 0 ; i < interests.size() ; i++) {
            final Interest interest = interests.get(i);
            final Chip chip = new Chip(ctx);
            chip.setPadding(isEditMode ? paddingDp : 0, paddingDp, paddingDp, paddingDp);
            chip.setText(interest.tag);
            if (isEditMode) {
                View.OnClickListener onClickListener = new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        askRemoveChip(interest);
                    }
                };
                chip.setOnClickListener(onClickListener);
                chip.setClickable(true);
                chip.setOnCloseIconClickListener(onClickListener);
                chip.setCloseIconVisible(true);
            }
            interestChipGroup.addView(chip);
        }

        if (isEditMode) {
            final Chip chip = new Chip(ctx);
            chip.setPadding(0, paddingDp, paddingDp, paddingDp);
            chip.setText("+ Add interest");
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    askAddChip();
                }
            };
            chip.setChipBackgroundColor(ctx.getResources().getColorStateList(R.color.colorPrimary));
            chip.setTextColor(ctx.getResources().getColorStateList(R.color.quantum_white_100));
            chip.setOnClickListener(onClickListener);
            chip.setClickable(true);
            interestChipGroup.addView(chip);
        }
    }

    private void askAddChip() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Add an interest to this trip");
        final EditText input = new EditText(ctx);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        builder.setView(input);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String tag = input.getText().toString();
                if (tag.trim().length() != 0) doAddInterest(tag);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        builder.show();
    }

    private void askRemoveChip(final Interest interest) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        doRemoveInterest(interest);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage("Confirm delete " + interest.tag + "?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void doAddInterest(final String tag) {
        if (tripId == -1) {
            interests.add(new Interest(tag));
            updateView();
        } else {
            final HashMap<String, Object> params = new HashMap<>();
            params.put("tag", tag);
            params.put("voyage", tripId);
            utils.callAPI("POST", "/interests", new JSONObject(params), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    interests.add(new Interest(tag));
                    updateView();
                }
            });
        }
    }

    private void doRemoveInterest(final Interest interest) {
        if (tripId == -1) {
            interests.remove(interest);
            updateView();
        } else {
            utils.callAPI("DELETE", "/interests/" + interest.id, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    interests.remove(interest);
                    updateView();
                }
            });
        }
    }

    public void doUploadAll(int tripId, AsyncResponse<Boolean> asyncResponse) {
        this.tripId = tripId;
        doUploadRecursive(0, asyncResponse);
    }

    private void doUploadRecursive(final int i, final AsyncResponse<Boolean> asyncResponse) {
        if (i >= interests.size()) {
            asyncResponse.run();
            return;
        }
        final HashMap<String, Object> params = new HashMap<>();
        params.put("tag", interests.get(i).tag);
        params.put("voyage", tripId);
        utils.callAPI("POST", "/interests", new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                doUploadRecursive(i + 1, asyncResponse);
            }
        });
    }
}
