package al1.esgi.app.pa.esgi2019.model;

import al1.esgi.app.pa.esgi2019.utils.SettingsManager;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Trip implements Serializable {
    public static Map<String, Trip> cache = new HashMap<>();
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public String id;
    public int idInt;
    public String title;
    public LocalDateTime start;
    public LocalDateTime end;
    public String startDateString = "(no info)";
    public String endDateString = "(no info)";
    public String description = "No description";
    public boolean close;
    public JsonElement picture;
    public String picture_url;
    public JsonArray usersLikes;
    public int likesCount = 0;
    public boolean isSelfLiked = false;
    public boolean isSelfCreated = false;
    public List<Step> steps = new ArrayList<>();
    public List<Interest> interests = new ArrayList<>();
    public JsonObject rawJsonData;

    public Trip(JsonObject tripObject) {
        rawJsonData = tripObject;

        this.id = tripObject.get("id").getAsString();
        this.idInt = tripObject.get("id").getAsInt();
        this.title = tripObject.get("title").getAsString();
        this.close = tripObject.get("close").getAsBoolean();

        try {
            for (JsonElement elem : tripObject.get("interests").getAsJsonArray()) {
                interests.add(new Interest(elem.getAsJsonObject()));
            }
        } catch (Exception e) {}

        try {
            int userCreatedId = tripObject.get("user").getAsJsonObject().get("id").getAsInt();
            isSelfCreated = userCreatedId == SettingsManager.currentUserId;
        } catch (Exception e) {
        }

        try {
            start = LocalDateTime.parse(tripObject.get("start").getAsString(), Utils.ISO_FORMATTER);
            startDateString = start.format(formatter);
        } catch (Exception e) {
        }

        try {
            end = LocalDateTime.parse(tripObject.get("end").getAsString(), Utils.ISO_FORMATTER);
            endDateString = end.format(formatter);
        } catch (Exception e) {
        }

        if (tripObject.get("etapes") != null) {
            for (JsonElement step : tripObject.get("etapes").getAsJsonArray()) {
                Step s = new Step(step.getAsJsonObject());
                s.trip = this;
                steps.add(s);
            }
        }

        try {
            description = tripObject.get("description").getAsString();
        } catch (Exception e) {
        }

        try {
            this.picture = new JsonObject();
            this.picture_url = tripObject.get("picture").getAsJsonObject().get("url").getAsString();
        } catch (Exception e) {
            this.picture = new JsonElement() {
                @Override
                public JsonElement deepCopy() {
                    return null;
                }
            };
        }

        try {
            this.end = LocalDateTime.parse(tripObject.get("end").getAsString(), Utils.ISO_FORMATTER);
        } catch (Exception e) {
        }

        if (tripObject.get("userslikes") != null) {
            this.usersLikes = tripObject.get("userslikes").getAsJsonArray();
            this.likesCount = usersLikes.size();
            for (JsonElement user : usersLikes) {
                if (user.getAsJsonObject().get("id").getAsInt() == SettingsManager.currentUserId) {
                    isSelfLiked = true;
                    break;
                }
            }
        } else {
            this.usersLikes = new JsonArray();
        }
    }

    public static Trip get(JsonObject tripObject) {
        String id = tripObject.get("id").getAsString();
        if (cache.containsKey(id)) {
            return cache.get(id);
        } else {
            Trip t = new Trip(tripObject);
            cache.put(id, t);
            return t;
        }
    }

    public static Trip get(String id) {
        if (cache.containsKey(id)) {
            return cache.get(id);
        } else {
            return null;
        }
    }

    public static void removeCache(String id) {
        cache.remove(id);
    }

    public User getUser() {
        return new User(rawJsonData.get("user").getAsJsonObject());
    }

    public void setUser(JsonObject user) {
    }

    public void toggleLike(User user) {
        if (isSelfLiked) {
            // DELETE the like
            for (JsonElement u : usersLikes) {
                if (u.getAsJsonObject().get("id").getAsInt() == Integer.parseInt(user.id)) {
                    likesCount--;
                    isSelfLiked = false;
                    break;
                }
            }
        } else {
            // ADD the like
            usersLikes.add(user.rawUserObject);
            likesCount++;
            isSelfLiked = true;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}





