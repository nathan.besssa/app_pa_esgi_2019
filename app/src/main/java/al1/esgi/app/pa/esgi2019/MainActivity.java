package al1.esgi.app.pa.esgi2019;

import al1.esgi.app.pa.esgi2019.home.HomeActivity;
import al1.esgi.app.pa.esgi2019.utils.SettingsManager;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Response;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    boolean isServerUp = false;
    int checkTimes = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                checkTimes++;
                if (isServerUp || checkTimes >= 12) exitSplash();
                else handler.postDelayed(this, 1000);
            }
        };
        handler.postDelayed(runnable, 2000);
        //(new SettingsManager(this)).clearAuth();
        Log.d("token", (new SettingsManager(this)).getToken());

        // make pulse animation for app logo
        final ImageView splash_circle = findViewById(R.id.splash_logo);
        final Animation anim = AnimationUtils.loadAnimation(this, R.anim.pulse);
        anim.setAnimationListener(
                new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        splash_circle.startAnimation(anim);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {
                    }
                });
        splash_circle.startAnimation(anim);

        // Here we wait for Heroku server to be booted up, then start the app
        Utils utils = new Utils(this);
        utils.callAPI("GET", "/", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                isServerUp = true;
            }
        });
    }

    private void exitSplash() {
        ImageView splash_circle = findViewById(R.id.splash_circle);
        splash_circle.setVisibility(View.VISIBLE);
        Animation anim = AnimationUtils.loadAnimation(this, R.anim.scale_up);
        splash_circle.startAnimation(anim);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                nextScreen();
            }
        }, 600);
    }

    private void nextScreen() {
        SettingsManager settings = new SettingsManager(this);
        Intent intent = settings.getToken().equals("none")
                ? new Intent(this, WelcomeActivity.class)
                : new Intent(this, HomeActivity.class);

        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        startActivity(intent);
        finish();
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}