package al1.esgi.app.pa.esgi2019.home;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.search.SearchActivity;
import al1.esgi.app.pa.esgi2019.adapter.TripCardAdapter;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.Constants;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import androidx.fragment.app.Fragment;
import com.daprlabs.cardstack.SwipeDeck;

import java.util.ArrayList;

public class DiscoveryFragment extends Fragment {
    public static boolean shouldReload = false;
    private OnFragmentDiscoverListener mListener;
    private DiscoveryUtils dcUtils;
    private TripCardAdapter adapter;
    private SwipeDeck cardStack;
    private ImageView searchBtnShow;
    private ImageView searchBtnClose;
    private ImageView searchBtnGo;
    private EditText searchInput;
    private LinearLayout searchBarLayout;
    private int seletedCardIndex = 0;
    private boolean searchBarShow = false;

    public DiscoveryFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.discovery_fragment, container, false);
        cardStack = view.findViewById(R.id.swipe_deck);
        searchBtnShow = view.findViewById(R.id.search_bar_btn_show);
        searchBtnClose = view.findViewById(R.id.search_bar_close);
        searchBtnGo = view.findViewById(R.id.search_bar_btn);
        searchInput = view.findViewById(R.id.search_bar_input);
        searchBarLayout = view.findViewById(R.id.search_bar_layout);
        searchBarLayout.setVisibility(View.GONE);
        loadData();

        View.OnClickListener toggleSearchBar = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchBarShow = !searchBarShow;
                searchBarLayout.setVisibility(searchBarShow ? View.VISIBLE : View.GONE);
            }
        };

        searchBtnShow.setOnClickListener(toggleSearchBar);
        searchBtnClose.setOnClickListener(toggleSearchBar);

        searchBtnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchActivity.prefillTerm = searchInput.getText().toString();
                Intent intent = new Intent(getContext(), SearchActivity.class);
                getActivity().startActivityForResult(intent, 1);
            }
        });


        cardStack.setEventCallback(new SwipeDeck.SwipeEventCallback() {
            @Override
            public void cardSwipedLeft(int position) {
                seletedCardIndex = position + 1;
            }

            @Override
            public void cardSwipedRight(int position) {
                seletedCardIndex = position + 1;
            }

            @Override
            public void cardsDepleted() {
                Log.i("MainActivity", "no more cards");
            }

            @Override
            public void cardActionDown() {
            }

            @Override
            public void cardActionUp() {
            }
        });
        return view;
    }

    private void loadData() {
        dcUtils = new DiscoveryUtils(this.getContext());
        dcUtils.getRecentTrips(new AsyncResponse<ArrayList<Trip>>() {
            @Override
            public void run() {
                adapter = new TripCardAdapter(
                        this.responseData, DiscoveryFragment.this.getContext());
                adapter.setLayout(R.layout.discovery_card, true, true);
                cardStack.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentDiscoverListener) {
            mListener = (OnFragmentDiscoverListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldReload) {
            loadData();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.REQUEST_CODE_VIEW_TRIP) {
            int NUMBER_OF_CARDS = 3;
            int childOffset = cardStack.getChildCount() - NUMBER_OF_CARDS + 1;
            final View child = cardStack.getChildAt(cardStack.getChildCount() - childOffset);
            adapter.renderView((Trip) adapter.getItem(seletedCardIndex), child);
        }
    }

    public interface OnFragmentDiscoverListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
