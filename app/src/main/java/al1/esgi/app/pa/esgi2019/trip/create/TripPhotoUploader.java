package al1.esgi.app.pa.esgi2019.trip.create;

import al1.esgi.app.pa.esgi2019.trip.TripStepData;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.PhotoUploader;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.util.Log;
import com.bikomobile.multipart.Multipart;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TripPhotoUploader {
    TripCreateActivity ac;
    PhotoUploader uploader;

    public TripPhotoUploader(final TripCreateActivity ac) {
        this.ac = ac;
        uploader = new PhotoUploader(ac);
    }

    public void runUploadPhotos(int tripId, AsyncResponse<Boolean> callback) {
        List<Multipart> list = serializePhotoData(tripId);
        recursiveUploadPhotos(list, 0, callback);
    }

    public void recursiveUploadPhotos(final List<Multipart> list, final int position, final AsyncResponse<Boolean> callback) {
        if (position == list.size()) {
            callback.run();
            return;
        }

        uploader.upload(list.get(position), new AsyncResponse<JsonObject>() {
            @Override
            public void run() {
                Log.d("trip", "uploaded photo " + position);
                recursiveUploadPhotos(list, position + 1, callback);
            }
        });
    }

    public List<Multipart> serializePhotoData(int tripId) {
        List<Multipart> list = new ArrayList<>();

        if (ac.tripPhoto != null) {
            Multipart m1 = new Multipart(ac);
            Map<String, String> p1 = new HashMap<>();
            m1.addFile("image/jpeg", "files", "image.jpg",
                    Utils.getFileDataFromBitmap(ac.tripPhoto));
            p1.put("refId", tripId + "");
            p1.put("ref", "voyage");
            p1.put("field", "picture");
            m1.addParams(p1);
            list.add(m1);
        }

        for (TripStepData cardView : ac.stepViews) {
            if (cardView.stepPhoto == null) continue;
            Multipart m = new Multipart(ac);
            Map<String, String> p = new HashMap<>();
            m.addFile("image/jpeg", "files", "image.jpg",
                    Utils.getFileDataFromBitmap(cardView.stepPhoto));
            p.put("refId", cardView.stepId + "");
            p.put("ref", "etape");
            p.put("field", "pictures");
            m.addParams(p);
            list.add(m);
        }

        return list;
    }
}
