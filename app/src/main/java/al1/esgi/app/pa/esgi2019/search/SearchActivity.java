package al1.esgi.app.pa.esgi2019.search;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.adapter.TripCardAdapter;
import al1.esgi.app.pa.esgi2019.model.Interest;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.utils.EncodingUtil;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.Response;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.json.JSONArray;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity {
    public static String prefillTerm = "";

    private Utils utils;
    private EditText input;
    private ProgressBar loading;
    private TextView notFound;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        utils = new Utils(this);
        input = findViewById(R.id.search_bar_input);
        loading = findViewById(R.id.loading);
        notFound = findViewById(R.id.text_not_found);
        listView = findViewById(R.id.listview_trips);

        input.setText(prefillTerm);
        loading.setVisibility(View.GONE);
        notFound.setVisibility(View.GONE);
        findViewById(R.id.search_bar_close).setVisibility(View.GONE);

        findViewById(R.id.search_bar_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                runSearch(input.getText().toString());
            }
        });

        runSearch(prefillTerm);
    }

    private void runSearch(String term) {
        loading.setVisibility(View.VISIBLE);
        notFound.setVisibility(View.GONE);
        utils.callAPIArray("GET", "/interests?tag=" + EncodingUtil.encodeURIComponent(term),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray jsonArray) {
                        ArrayList<Interest> interests = tripJSONResponseToArray(jsonArray);
                        if (interests.size() == 0) {
                            notFound.setVisibility(View.VISIBLE);
                        }
                        ArrayList<Trip> trips = new ArrayList<>();
                        for (Interest in : interests) {
                            if (in.trip != null) trips.add(in.trip);
                        }
                        loading.setVisibility(View.GONE);
                        final TripCardAdapter adapter = new TripCardAdapter(trips, SearchActivity.this);
                        adapter.setLayout(R.layout.profile_trip_card, false, true);
                        listView.setAdapter(adapter);
                    }
                });
    }

    public ArrayList<Interest> tripJSONResponseToArray(JSONArray response) {
        JsonArray jsonArray = new JsonParser().parse(response.toString()).getAsJsonArray();
        ArrayList<Interest> interests = new ArrayList<>();
        for (JsonElement element : jsonArray) {
            interests.add(new Interest(element.getAsJsonObject()));
        }
        return interests;
    }
}
