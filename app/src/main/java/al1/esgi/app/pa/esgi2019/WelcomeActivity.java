package al1.esgi.app.pa.esgi2019;

import al1.esgi.app.pa.esgi2019.auth.LogInActivity;
import al1.esgi.app.pa.esgi2019.auth.SignUpActivity;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import com.yqritc.scalablevideoview.ScalableVideoView;

import java.io.IOException;

public class WelcomeActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_activity);

        final ScalableVideoView mVideoView = findViewById(R.id.video_view);
        try {
            mVideoView.setRawData(R.raw.landscape_loop);
            mVideoView.setVolume(0, 0);
            mVideoView.setLooping(true);
            mVideoView.prepareAsync(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mVideoView.start();
                }
            });
        } catch (IOException ioe) {
            //Log.e("app", "Cannot start video");
        }

        Button login_btn = findViewById(R.id.btn_login);
        Button register_btn = findViewById(R.id.btn_sign_up);

        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openActivity(WelcomeActivity.this, LogInActivity.class);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

        register_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Utils.openActivity(WelcomeActivity.this, SignUpActivity.class);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });

    }
}
