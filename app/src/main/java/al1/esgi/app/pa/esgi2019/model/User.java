package al1.esgi.app.pa.esgi2019.model;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

public class User {
    public String id;
    public String fullname;
    public String username;
    public String email;
    public String blocked;
    public String firstname;
    public String lastname;
    public JsonElement picture;
    public String picture_url;
    public String phone;
    public JsonArray users;
    public JsonArray voyages;
    public List<Interest> interests = new ArrayList<>();
    public JsonObject rawUserObject;

    public List<Integer> likedPostIds = new ArrayList<>();
    public List<Integer> followUserIds = new ArrayList<>();

    public User(JsonObject userObject) {
        this.rawUserObject = userObject;
        this.id = userObject.get("id").getAsString();
        this.username = userObject.get("username").getAsString();
        this.email = userObject.get("email").getAsString();
        this.blocked = userObject.get("blocked").getAsString();
        this.firstname = userObject.get("firstname").getAsString();
        this.lastname = userObject.get("lastname").getAsString();

        try {
            this.phone = userObject.get("phone").getAsString();
        } catch (Exception e) {
            this.phone = "(no phone number)";
        }

        try {
            this.users = userObject.get("users").getAsJsonArray();
            this.voyages = userObject.get("voyages").getAsJsonArray();
        } catch (Exception e) {
        }

        if (userObject.get("picture") == null) {
            this.picture = new JsonObject();
        } else {
            this.picture = userObject.get("picture").getAsJsonObject();
        }

        try {
            this.picture_url = userObject.get("picture").getAsJsonObject().get("url").getAsString();
        } catch (Exception e) {
            this.picture_url = "https://img.icons8.com/bubbles/344/user.png";
        }

        if (userObject.get("interests") != null) {
            for (JsonElement element : userObject.get("interests").getAsJsonArray()) {
                interests.add(new Interest(element.getAsJsonObject()));
            }
        }

        if (userObject.get("likes") != null) {
            for (JsonElement element : userObject.get("likes").getAsJsonArray()) {
                likedPostIds.add(element.getAsJsonObject().get("id").getAsInt());
            }
        }

        fullname = firstname + " " + lastname;

        try {
            JsonArray follows = userObject.get("follows").getAsJsonArray();
            for (JsonElement elem : follows) {
                followUserIds.add(elem.getAsJsonObject().get("id").getAsInt());
            }
        } catch (Exception e) {
        }
    }
}




