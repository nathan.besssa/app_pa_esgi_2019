package al1.esgi.app.pa.esgi2019.utils;

import al1.esgi.app.pa.esgi2019.model.Trip;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;
import com.android.volley.NetworkResponse;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bikomobile.multipart.Multipart;
import com.bikomobile.multipart.MultipartRequest;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;

public class PhotoUploader {
    public static int PROFILE_PHOTO_UPLOAD = 1001;
    public static int TRIP_PHOTO_UPLOAD = 1002;
    public Bitmap bitmapPhoto;
    public Trip trip;
    private Context ctx;
    private SettingsManager settings;

    public PhotoUploader(Context ctx) {
        this.ctx = ctx;
        this.settings = new SettingsManager(ctx);
    }

    public void showPhotoSelector(Activity ac) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ac.startActivityForResult(intent, PROFILE_PHOTO_UPLOAD);
    }

    public void onActivityResultHandler(Activity ac, int requestCode, int resultCode,
                                        Intent data, AsyncResponse<JsonObject> asyncResponse) throws IOException {
        if (resultCode != RESULT_OK || data == null) return;
        if (requestCode == PROFILE_PHOTO_UPLOAD) {
            Toast.makeText(ctx, "Uploading photo...", Toast.LENGTH_SHORT).show();
            Uri path = data.getData();
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(ac.getContentResolver(), path);
            Map<String, String> params = new HashMap<>();
            params.put("refId", settings.getUid() + "");
            params.put("ref", "user");
            params.put("source", "users-permissions");
            params.put("field", "picture");
            upload(bitmap, params, asyncResponse);
        }
    }

    public void asyncUploadTripPhoto(AsyncResponse<JsonObject> asyncResponse) {
        if (trip == null || bitmapPhoto == null) return;
        Map<String, String> params = new HashMap<>();
        params.put("refId", trip.id);
        params.put("ref", "voyage");
        params.put("field", "picture");
        upload(bitmapPhoto, params, asyncResponse);
    }

    public void upload(final Bitmap bitmap, Map<String, String> params, final AsyncResponse<JsonObject> asyncResponse) {
        String url = Constants.api("/upload");

        Multipart multipart = new Multipart(ctx);
        multipart.addFile("image/jpeg", "files", "image.jpg", Utils.getFileDataFromBitmap(bitmap));
        multipart.addParams(params);

        Map<String, String> headers = new HashMap<>();
        String token = settings.getToken();
        headers.put("Authorization", "Bearer " + token);

        MultipartRequest multipartRequest = multipart.getRequest(url, headers, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                JsonArray arr = new JsonParser().parse(new String(response.data)).getAsJsonArray();
                asyncResponse.setResponseData(arr.get(0).getAsJsonObject());
                asyncResponse.run();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                asyncResponse.setResponseData(null);
                asyncResponse.run();
                Log.e("upload", "error: " + error.toString());
            }
        });

        MySingleton.getInstance(ctx).addToRequestQueue(multipartRequest);
    }

    public void upload(final Multipart multipart, final AsyncResponse<JsonObject> asyncResponse) {
        String url = Constants.api("/upload");

        Map<String, String> headers = new HashMap<>();
        String token = settings.getToken();
        headers.put("Authorization", "Bearer " + token);

        MultipartRequest multipartRequest = multipart.getRequest(url, headers, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response) {
                JsonArray arr = new JsonParser().parse(new String(response.data)).getAsJsonArray();
                asyncResponse.setResponseData(arr.get(0).getAsJsonObject());
                asyncResponse.run();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                asyncResponse.setResponseData(null);
                asyncResponse.run();
                Log.e("upload", "error: " + error.toString());
            }
        });

        MySingleton.getInstance(ctx).addToRequestQueue(multipartRequest);
    }
}
