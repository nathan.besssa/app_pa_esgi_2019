package al1.esgi.app.pa.esgi2019.utils;

public class Constants {
    public final static int REQUEST_CODE_VIEW_TRIP = 9001;
    public final static int REQUEST_CODE_NEW_TRIP = 9002;
    public final static int REQUEST_CODE_SELECT_TRIP_PHOTO = 1003;
    public final static int REQUEST_CODE_SELECT_STEP_PHOTO = 1004;
    public final static int REQUEST_CODE_SELECT_TRIP_LOCATION = 1005;

    public static String api(String endpoint) {
        return "https://dev-tripadlowcost.herokuapp.com" + endpoint;
    }
}
