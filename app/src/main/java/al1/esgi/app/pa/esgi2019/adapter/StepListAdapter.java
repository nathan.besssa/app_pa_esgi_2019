package al1.esgi.app.pa.esgi2019.adapter;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.step.StepEditActivity;
import al1.esgi.app.pa.esgi2019.model.Step;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import com.squareup.picasso.Picasso;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.List;

public class StepListAdapter extends ArrayAdapter<Step> {
    public Activity ac;
    private int layoutId = 0;
    private Utils utils;

    public StepListAdapter(@NonNull Context context, int resource, @NonNull List<Step> objects) {
        super(context, resource, objects);
        layoutId = resource;
        utils = new Utils(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(layoutId, parent, false);
        }

        final Step step = getItem(position);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        ImageView imageView = v.findViewById(R.id.step_image);
        TextView tvTitle = v.findViewById(R.id.step_title);
        TextView tvDesc = v.findViewById(R.id.step_desc);
        RatingBar mark = v.findViewById(R.id.step_mark);
        ImageButton btnMenu = v.findViewById(R.id.step_btn_menu);

        if (step.picture_url != null) {
            String trip_pic_url = step.picture_url;
            Picasso.get().load(trip_pic_url).into(imageView);
        } else {
            imageView.setImageResource(R.drawable.bg_img_placeholder);
        }

        tvTitle.setText(step.title);
        tvDesc.setText(
                "Start: " + step.start.format(formatter)
                        + "\nAddress: " + step.address
                        + "\n-------\n" + step.comment
        );
        mark.setRating(step.mark);

        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] colors = {"Modify", "Delete"};

                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle(step.title);
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(ac.getApplicationContext(), StepEditActivity.class);
                            StepEditActivity.step = step;
                            StepEditActivity.trip = step.trip;
                            ac.startActivityForResult(intent, 1);
                        } else if (which == 1) {
                            askDelete(step);
                        }
                    }
                });
                builder.show();
            }
        });

        if (!step.trip.isSelfCreated) btnMenu.setVisibility(View.GONE);

        return v;
    }

    private void askDelete(final Step step) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        step.trip.steps.remove(step);
                        StepListAdapter.this.notifyDataSetInvalidated();
                        Log.d("callAPI", "/etapes/" + step.id);
                        utils.callAPI("DELETE", "/etapes/" + step.id);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(ac);
        builder.setMessage("Do you really want to delete this step?")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }
}
