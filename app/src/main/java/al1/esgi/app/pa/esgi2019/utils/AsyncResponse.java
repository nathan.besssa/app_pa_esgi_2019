package al1.esgi.app.pa.esgi2019.utils;

public class AsyncResponse<T> implements Runnable {
    public T responseData;

    public AsyncResponse<T> setResponseData(T responseData) {
        this.responseData = responseData;
        return this;
    }

    public void run() {}
}
