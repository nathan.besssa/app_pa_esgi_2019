package al1.esgi.app.pa.esgi2019.utils;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ListAdapter;
import android.widget.ListView;
import androidx.annotation.RequiresApi;

public class NonScrollListView extends ListView {
    public NonScrollListView(Context context) {
        super(context);
    }

    public NonScrollListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NonScrollListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public NonScrollListView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    /**
     * Measure the height of all the items in the list and set that to be the height of this
     * view, so it appears as full size and doesn't need to scroll.
     *
     * @param widthMeasureSpec
     * @param heightMeasureSpec
     */
    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        ListAdapter adapter = this.getAdapter();
        if (adapter == null) {
            // we don't have an adapter yet, so probably initializing.
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            return;
        }

        int totalHeight = 0;

        // compute the height of all the items
        int itemCount = adapter.getCount();
        for (int index = 0; index < itemCount; index++) {
            View item = adapter.getView(index, null, this);
            // set the width so it can figure out the height
            item.measure(widthMeasureSpec, 0);
            totalHeight += item.getMeasuredHeight();
        }

        // add any dividers to the height
        if (this.getDividerHeight() > 0) {
            totalHeight += this.getDividerHeight() * Math.max(0, itemCount - 1);
        }

        // make it so
        //this.setMeasuredDimension(widthMeasureSpec,
        super.onMeasure(widthMeasureSpec,
                MeasureSpec.makeMeasureSpec(totalHeight, MeasureSpec.EXACTLY));
    }
}