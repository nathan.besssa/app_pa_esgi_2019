package al1.esgi.app.pa.esgi2019.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ScrollView;
import android.widget.Toast;
import androidx.core.content.ContextCompat;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONArray;
import org.json.JSONObject;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;
import org.threeten.bp.format.DateTimeFormatterBuilder;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

public class Utils {
    public final static DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    public static DateTimeFormatter ISO_FORMATTER = new DateTimeFormatterBuilder()
            .append(DateTimeFormatter.ISO_LOCAL_DATE_TIME)
            .optionalStart().appendOffset("+HH:MM", "+00:00").optionalEnd()
            .optionalStart().appendOffset("+HHMM", "+0000").optionalEnd()
            .optionalStart().appendOffset("+HH", "Z").optionalEnd()
            .toFormatter();
    private Context ctx;
    private SettingsManager settings;

    public Utils(Context ctx) {
        this.ctx = ctx;
        this.settings = new SettingsManager(ctx);
    }

    public static void openActivity(Activity ac, Class<?> cls) {
        Intent intent = new Intent(ac.getApplicationContext(), cls);
        ac.startActivity(intent);
    }

    public static void replaceActivity(Activity ac, Class<?> cls) {
        Intent intent = new Intent(ac.getApplicationContext(), cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        ac.startActivity(intent);
        ac.finish();
    }

    public static void clearAndOpenActivity(Activity ac, Class<?> cls) {
        Intent intent = new Intent(ac.getApplicationContext(), cls);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        ac.startActivity(intent);
        ac.finish();
    }

    public static JsonObject toGSON(JSONObject obj) {
        return new JsonParser().parse(obj.toString()).getAsJsonObject();
    }

    /**
     * Turn drawable resource into byte array.
     *
     * @param context parent context
     * @param id      drawable resource id
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, int id) {
        Drawable drawable = ContextCompat.getDrawable(context, id);
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Turn drawable into byte array.
     *
     * @param drawable data
     * @return byte array
     */
    public static byte[] getFileDataFromDrawable(Context context, Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    /**
     * Turn bitmap into byte array.
     *
     * @param bitmap bitmap
     * @return byte array
     */
    public static byte[] getFileDataFromBitmap(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    public static View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    public static void scrollToTop(final Activity ac, final int scrollViewId) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ScrollView sv = ac.findViewById(scrollViewId);
                sv.scrollTo(0, 0);
            }
        }, 100);
    }

    public static void setListViewHeightBasedOnChildren(ListView listView) {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }

        int totalHeight = 0;
        for (int i = 0; i < listAdapter.getCount(); i++) {
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            totalHeight += listItem.getMeasuredHeight();
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + (listView.getDividerHeight() * (listAdapter.getCount() - 1));
        listView.setLayoutParams(params);
        listView.requestLayout();
    }

    public static String toISODate(String input) {
        LocalDate ld = LocalDate.parse(input, DATEFORMATTER);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return ld.format(formatter);
    }

    public static void toast(Context ctx, String msg) {
        Toast.makeText(ctx, msg, Toast.LENGTH_SHORT).show();
    }

    public void callAPI(String method, String endpoint) {
        callAPI(method, endpoint, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("callAPI response", response.toString());
            }
        });
    }

    public void callAPI(String method, String endpoint, Listener<JSONObject> listener) {
        callAPI(method, endpoint, null, listener);
    }

    public void callAPI(String method, String endpoint, JSONObject jsonObject, final Listener<JSONObject> listener) {
        final String url = Constants.api(endpoint);
        int httpMethod = Request.Method.GET;
        if (method.equals("POST")) httpMethod = Request.Method.POST;
        if (method.equals("PUT")) httpMethod = Request.Method.PUT;
        if (method.equals("DELETE")) httpMethod = Request.Method.DELETE;
        //Log.d("method", "" + httpMethod);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                httpMethod,
                url,
                jsonObject,
                listener, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("api", "Error: " + error.getMessage());
                //Toast.makeText(ctx, "API error", Toast.LENGTH_SHORT).show();
                listener.onResponse(null);
            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String token = settings.getToken();
                if (!token.equals("none")) {
                    headers.put("Authorization", "Bearer " + token);
                }
                return headers;
            }
        };
        MySingleton.getInstance(ctx).addToRequestQueue(jsonObjectRequest);
    }

    public void callAPIArray(String method, String endpoint, Listener<JSONArray> listener) {
        final String url = Constants.api(endpoint);
        final SettingsManager settings = new SettingsManager(ctx);
        int httpMethod = Request.Method.GET;
        if (method.equals("POST")) httpMethod = Request.Method.POST;
        if (method.equals("PUT")) httpMethod = Request.Method.PUT;
        if (method.equals("DELETE")) httpMethod = Request.Method.DELETE;

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest(
                httpMethod,
                url,
                null,
                listener, new ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("api", "Error: " + error.getMessage());
                Toast.makeText(ctx, "API error", Toast.LENGTH_SHORT).show();
            }
        }
        ) {
            @Override
            public Map<String, String> getHeaders() {
                Map<String, String> headers = new HashMap<>();
                String token = settings.getToken();
                if (!token.equals("none")) {
                    headers.put("Authorization", "Bearer " + token);
                }
                return headers;
            }
        };
        MySingleton.getInstance(ctx).addToRequestQueue(jsonObjectRequest);
    }
}
