package al1.esgi.app.pa.esgi2019.trip.create;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.trip.TripInterestUtils;
import al1.esgi.app.pa.esgi2019.trip.TripStepData;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.Constants;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Response;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class TripCreateActivity extends AppCompatActivity implements View.OnClickListener {
    public TripStepData selectedStepForLocationPicker;
    public TripStepData currentStepData;
    public TripInterestUtils tripInterestUtils;

    public Bitmap tripPhoto;
    public LinearLayout tripStepList;
    public List<TripStepData> stepViews = new ArrayList<>();
    private ImageView tripAddPhoto;
    private TextView tripAddBtnAddStep;
    private Utils utils;
    private LinearLayout tripSubmitLoading;
    private TextView tripSubmitLoadingText;
    private ChipGroup tripInterestChipGroup;

    // helper classes
    private StepCard tripCreateStepCard;
    private TripPhotoUploader tripCreatePhotoUploader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_create_activity);

        tripInterestUtils = new TripInterestUtils(this);
        tripCreateStepCard = new StepCard(this);
        tripCreatePhotoUploader = new TripPhotoUploader(this);
        utils = new Utils(this);

        tripStepList = findViewById(R.id.trip_add_step_list);
        tripAddPhoto = findViewById(R.id.trip_add_photo);
        tripAddBtnAddStep = findViewById(R.id.trip_add_btn_add_step);
        tripAddPhoto.setOnClickListener(this);
        tripAddBtnAddStep.setOnClickListener(this);
        findViewById(R.id.trip_add_btn_done).setOnClickListener(this);
        tripSubmitLoading = findViewById(R.id.trip_add_loading);
        tripSubmitLoadingText = findViewById(R.id.trip_add_loading_text);
        tripInterestChipGroup = findViewById(R.id.interests_chip_group);

        tripInterestUtils.setViewGroup(tripInterestChipGroup, true);
        tripInterestUtils.updateView();
        setupDatePicker((EditText) findViewById(R.id.trip_add_start), findViewById(R.id.trip_add_start_select));
        setupDatePicker((EditText) findViewById(R.id.trip_add_end), findViewById(R.id.trip_add_end_select));
    }

    private String getTripInputTitle() {
        return ((EditText) findViewById(R.id.trip_add_title)).getText().toString();
    }

    private String getTripInputDesc() {
        return ((EditText) findViewById(R.id.trip_add_desc)).getText().toString();
    }

    public void showPhotoSelector() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_TRIP_PHOTO);
    }

    public void showLocationPicker(TripStepData v) {
        Intent intent = new Intent(this, LocationPicker.class);
        selectedStepForLocationPicker = v;
        startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_TRIP_LOCATION);
    }

    /**
     * SUBMIT CODE
     */
    private void submit() {
        String startDate = ((EditText) findViewById(R.id.trip_add_start)).getText().toString();
        String endDate = ((EditText) findViewById(R.id.trip_add_end)).getText().toString();

        if (isEmpty("Trip title", getTripInputTitle())) return;
        if (isEmpty("Trip start date", startDate)) return;
        if (isEmpty("Trip end date", endDate)) return;
        if (tripCreateStepCard.hasEmptyCard()) return;

        final HashMap<String, Object> params = new HashMap<>();
        params.put("close", false);
        params.put("title", getTripInputTitle());
        params.put("description", getTripInputDesc());
        params.put("start", Utils.toISODate(startDate));
        params.put("end", Utils.toISODate(endDate));

        tripSubmitLoading.setVisibility(View.VISIBLE);
        tripSubmitLoadingText.setText("Please wait...");
        utils.callAPI("POST", "/voyages", new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JsonObject obj = Utils.toGSON(response);
                    if (obj.get("id") == null) {
                        throw new Exception();
                    }
                    int tripId = obj.get("id").getAsInt();
                    tripSubmitLoadingText.setText("Saving steps...");
                    submitInterests(tripId);
                } catch (Exception e) {
                    tripSubmitLoading.setVisibility(View.GONE);
                    Toast.makeText(TripCreateActivity.this, "ERROR, can't create new trip", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void submitInterests(final int tripId) {
        tripInterestUtils.doUploadAll(tripId, new AsyncResponse<Boolean>() {
            @Override
            public void run() {
                submitSteps(tripId);
            }
        });
    }

    private void submitSteps(final int tripId) {
        tripCreateStepCard.runUploadSteps(tripId, new AsyncResponse<Boolean>() {
            @Override
            public void run() {
                tripSubmitLoadingText.setText("Saving photos...");
                submitPhotos(tripId);
            }
        });
    }

    private void submitPhotos(int tripId) {
        tripCreatePhotoUploader.runUploadPhotos(tripId, new AsyncResponse<Boolean>() {
            @Override
            public void run() {
                Toast.makeText(TripCreateActivity.this, "Trip created!", Toast.LENGTH_SHORT).show();
                Intent returnIntent = new Intent();
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    private boolean isEmpty(String name, String content) {
        if (content == null || content.trim().length() == 0) {
            Utils.toast(this, name + " cannot be empty!");
            return true;
        } else {
            return false;
        }
    }

    /**
     * END OF SUBMIT CODE
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.trip_add_photo:
                showPhotoSelector();
                break;

            case R.id.trip_add_btn_add_step:
                tripCreateStepCard.createCard();
                break;

            case R.id.trip_add_btn_done:
                submit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE_SELECT_TRIP_PHOTO && resultCode == RESULT_OK && data != null) {
            try {
                Uri path = data.getData();
                tripPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                tripAddPhoto.setImageBitmap(tripPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.REQUEST_CODE_SELECT_STEP_PHOTO && resultCode == RESULT_OK && data != null) {
            try {
                Uri path = data.getData();
                Bitmap stepPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                currentStepData.stepPhoto = stepPhoto;
                ((ImageView) currentStepData.findViewById(R.id.step_image)).setImageBitmap(stepPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.REQUEST_CODE_SELECT_TRIP_LOCATION && resultCode == RESULT_OK && data != null) {
            try {
                tripCreateStepCard.updateLocation(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setupDatePicker(final EditText inputView, View btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(TripCreateActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int mm0, int dd) {
                        int mm = mm0 + 1;
                        inputView.setText((dd < 10 ? "0" : "") + dd + "/"
                                + (mm < 10 ? "0" : "") + mm + "/" + yyyy);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });
    }
}
