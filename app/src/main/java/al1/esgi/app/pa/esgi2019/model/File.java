package al1.esgi.app.pa.esgi2019.model;

public class File {
    public String id;
    public String name;
    public String hash;
    public String sha256;
    public String ext;
    public String mime;
    public String size;
    public String url;
    public String provider;
    public String public_id;
    public String related;

}
