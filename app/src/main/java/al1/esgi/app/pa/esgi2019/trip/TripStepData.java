package al1.esgi.app.pa.esgi2019.trip;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import androidx.annotation.RequiresApi;

public class TripStepData extends LinearLayout {
    public Bitmap stepPhoto;
    public int stepId = 0;
    public double lat = 0;
    public double lng = 0;

    public TripStepData(Context context) {
        super(context);
    }

    public TripStepData(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TripStepData(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public TripStepData(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
