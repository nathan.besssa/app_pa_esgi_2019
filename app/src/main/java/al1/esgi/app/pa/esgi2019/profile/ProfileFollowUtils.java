package al1.esgi.app.pa.esgi2019.profile;

import al1.esgi.app.pa.esgi2019.model.User;
import al1.esgi.app.pa.esgi2019.utils.SettingsManager;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.view.View;
import android.widget.Toast;
import com.android.volley.Response;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class ProfileFollowUtils {
    ProfileFragment ac;
    Utils utils;

    public ProfileFollowUtils(ProfileFragment ac) {
        this.ac = ac;
        this.utils = new Utils(ac.getContext());
    }

    public void renderFollowButton(final User user) {
        if (isFollowed(user)) {
            ac.btnFollow.setVisibility(View.GONE);
            ac.btnFollowed.setVisibility(View.VISIBLE);
        } else {
            ac.btnFollow.setVisibility(View.VISIBLE);
            ac.btnFollowed.setVisibility(View.GONE);
        }

        ac.btnFollow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFollow(user, true);
            }
        });

        ac.btnFollowed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFollow(user, false);
            }
        });
    }

    public boolean isFollowed(User user) {
        for (int id : SettingsManager.currentUser.followUserIds) {
            if (id == Integer.parseInt(user.id)) return true;
        }
        return false;
    }

    private void setFollow(final User user, boolean enable) {
        Integer uid = Integer.valueOf(user.id);
        if (enable) SettingsManager.currentUser.followUserIds.add(uid);
        else SettingsManager.currentUser.followUserIds.remove(uid);

        JSONArray followsArray = new JSONArray();
        for (int id : SettingsManager.currentUser.followUserIds) {
            followsArray.put(id);
        }
        HashMap<String, Object> params = new HashMap<>();
        params.put("follows", followsArray);

        utils.callAPI("PUT", "/users/" + SettingsManager.currentUserId, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response == null) {
                    Toast.makeText(ac.getContext(), "Server error", Toast.LENGTH_SHORT).show();
                } else {
                    renderFollowButton(user);
                }
            }
        });
    }
}
