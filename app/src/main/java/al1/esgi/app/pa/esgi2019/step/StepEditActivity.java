package al1.esgi.app.pa.esgi2019.step;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.trip.TripStepData;
import al1.esgi.app.pa.esgi2019.trip.create.LocationPicker;
import al1.esgi.app.pa.esgi2019.model.Step;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.Constants;
import al1.esgi.app.pa.esgi2019.utils.PhotoUploader;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Response;
import com.bikomobile.multipart.Multipart;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class StepEditActivity extends AppCompatActivity {
    public static Step step;
    public static Trip trip;
    LinearLayout tripStepList;
    TripStepData tripStepData;
    PhotoUploader uploader;
    EditText inputTitle;
    EditText inputStart;
    EditText inputAddress;
    EditText inputComment;
    RatingBar inputMark;
    ImageView selectPhoto;
    Button saveButton;
    Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.step_edit_activity);
        tripStepList = findViewById(R.id.trip_add_step_list);

        tripStepData = (TripStepData) this.getLayoutInflater().inflate(R.layout.trip_create_step_item, null);
        final TripStepData v = tripStepData;
        setupDatePicker((EditText) v.findViewById(R.id.step_start), v.findViewById(R.id.step_start_select));
        tripStepList.addView(v);
        v.findViewById(R.id.step_btn_delete).setVisibility(View.GONE);
        selectPhoto = tripStepData.findViewById(R.id.step_image);
        selectPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPhotoSelector();
            }
        });

        v.findViewById(R.id.step_btn_pick_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLocationPicker();
            }
        });

        utils = new Utils(this);
        uploader = new PhotoUploader(this);
        inputTitle = v.findViewById(R.id.step_title);
        inputStart = v.findViewById(R.id.step_start);
        inputAddress = v.findViewById(R.id.step_adress);
        inputComment = v.findViewById(R.id.step_comment);
        inputMark = v.findViewById(R.id.step_mark);
        saveButton = findViewById(R.id.step_btn_save);

        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onSubmit();
            }
        });

        if (isEditMode()) {
            populateDataIntoView();
            ((TextView) findViewById(R.id.step_activity_title)).setText("Modify step");
        } else {
            ((TextView) findViewById(R.id.step_activity_title)).setText("Add step");
        }
    }

    private void populateDataIntoView() {
        inputTitle.setText(step.title);
        inputStart.setText(step.start.format(Utils.DATEFORMATTER));
        inputAddress.setText(step.address);
        inputComment.setText(step.comment);
        inputMark.setRating(step.mark);
        tripStepData.stepId = step.id;
        tripStepData.lat = step.lat;
        tripStepData.lng = step.lng;

        if (step.picture_url != null) {
            String trip_pic_url = step.picture_url;
            Picasso.get().load(trip_pic_url).into(selectPhoto);
        } else {
            selectPhoto.setImageResource(R.drawable.bg_upload_placeholder);
        }
    }

    private void showPhotoSelector() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_STEP_PHOTO);
    }

    private void showLocationPicker() {
        Intent intent = new Intent(this, LocationPicker.class);
        startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_TRIP_LOCATION);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE_SELECT_STEP_PHOTO && resultCode == RESULT_OK && data != null) {
            try {
                Uri path = data.getData();
                Bitmap stepPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                tripStepData.stepPhoto = stepPhoto;
                ((ImageView) tripStepData.findViewById(R.id.step_image)).setImageBitmap(stepPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (requestCode == Constants.REQUEST_CODE_SELECT_TRIP_LOCATION && resultCode == RESULT_OK && data != null) {
            try {
                updateLocation(data);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateLocation(Intent intent) {
        TripStepData v = tripStepData;
        v.lat = intent.getDoubleExtra("lat", 0);
        v.lng = intent.getDoubleExtra("lng", 0);
        EditText addr = v.findViewById(R.id.step_adress);
        addr.setText(intent.getStringExtra("full_address"));
    }

    private void setupDatePicker(final EditText inputView, View btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(StepEditActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int mm0, int dd) {
                        int mm = mm0 + 1;
                        inputView.setText((dd < 10 ? "0" : "") + dd + "/"
                                + (mm < 10 ? "0" : "") + mm + "/" + yyyy);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });
    }

    private void onSubmit() {
        saveButton.setEnabled(false);
        saveButton.setText("Please wait...");

        String startDate = inputStart.getText().toString();
        final HashMap<String, Object> params = new HashMap<>();
        params.put("adress", inputAddress.getText().toString());
        params.put("comment", inputComment.getText().toString());
        params.put("mark", (int) inputMark.getRating());
        params.put("start", Utils.toISODate(startDate));
        params.put("title", inputTitle.getText().toString());
        params.put("voyage", Integer.parseInt(trip.id));

        Response.Listener<JSONObject> callback = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JsonObject res = Utils.toGSON(response);
                try {
                    Step mStep = new Step(res);
                    mStep.trip = trip;
                    uploadPhoto(mStep);
                } catch (Exception e) {
                    onSubmissionDone(null);
                }
            }
        };

        if (isEditMode()) {
            utils.callAPI("PUT", "/etapes/" + step.id, new JSONObject(params), callback);
        } else {
            utils.callAPI("POST", "/etapes", new JSONObject(params), callback);
        }
    }

    private void uploadPhoto(final Step mStep) {
        if (tripStepData.stepPhoto == null) {
            onSubmissionDone(mStep);
            return;
        }

        Multipart m1 = new Multipart(this);
        Map<String, String> p1 = new HashMap<>();
        m1.addFile("image/jpeg", "files", "image.jpg",
                Utils.getFileDataFromBitmap(tripStepData.stepPhoto));
        p1.put("refId", mStep.id + "");
        p1.put("ref", "etape");
        p1.put("field", "pictures");
        m1.addParams(p1);

        final Response.Listener<JSONObject> callback = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                JsonObject res = Utils.toGSON(response);
                try {
                    Step mStep1 = new Step(res);
                    mStep1.trip = trip;
                    onSubmissionDone(mStep1);
                } catch (Exception e) {
                    onSubmissionDone(null);
                }
            }
        };

        uploader.upload(m1, new AsyncResponse<JsonObject>() {
            @Override
            public void run() {
                utils.callAPI("GET", "/etapes/" + mStep.id, null, callback);
            }
        });
    }

    private void onSubmissionDone(Step mStep) {
        if (mStep != null) {
            Utils.toast(this, "Step Added!");
            int idx = trip.steps.indexOf(step);
            if (idx == -1) trip.steps.add(mStep);
            else trip.steps.set(idx, mStep);
            finish();
        } else {
            saveButton.setEnabled(true);
            saveButton.setText("Save");
            Utils.toast(this, "ERROR! Please try again later.");
        }
    }

    private boolean isEditMode() {
        return step != null;
    }
}
