package al1.esgi.app.pa.esgi2019.home;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;

public class HomeViewPager extends ViewPager {
    private boolean enabled;

    public HomeViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public HomeViewPager(@NonNull Context context) {
        super(context);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return false;
    }
}
