package al1.esgi.app.pa.esgi2019.model;

import al1.esgi.app.pa.esgi2019.utils.Utils;
import com.google.gson.JsonObject;
import org.threeten.bp.LocalDateTime;

public class Step {
    public int id;
    public String title;
    public String address  = "";
    public String picture_url;
    public int mark;
    public String comment = "";
    public LocalDateTime start;
    public double lat;
    public double lng;
    public Trip trip;

    public Step(JsonObject stepObject) {
        this.id = stepObject.get("id").getAsInt();
        this.title = stepObject.get("title").getAsString();

        try {
            this.mark = stepObject.get("mark").getAsInt();
            this.mark = Math.min(5, mark); // limit to 5 stars
        } catch (Exception e) {
            this.mark = 0;
        }

        try {
            this.comment = stepObject.get("comment").getAsString();
        } catch (Exception e) {}

        try {
            this.address = stepObject.get("adress").getAsString();
        } catch (Exception e) {}

        try {
            this.picture_url = stepObject.get("pictures")
                    .getAsJsonArray().get(0).getAsJsonObject()
                    .get("url").getAsString();
        } catch (Exception e) {}

        try {
            this.start = LocalDateTime.parse(stepObject.get("start").getAsString(),
                    Utils.ISO_FORMATTER);
        } catch (Exception e) {}

        try {
            this.lat = stepObject.get("lat").getAsDouble();
            this.lng = stepObject.get("lon").getAsDouble();
        } catch (Exception e) {}
    }
}
