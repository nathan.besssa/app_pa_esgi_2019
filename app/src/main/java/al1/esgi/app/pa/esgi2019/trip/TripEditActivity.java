package al1.esgi.app.pa.esgi2019.trip;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.home.DiscoveryFragment;
import al1.esgi.app.pa.esgi2019.profile.ProfileFragment;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.Constants;
import al1.esgi.app.pa.esgi2019.utils.PhotoUploader;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Response;
import com.bikomobile.multipart.Multipart;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class TripEditActivity extends AppCompatActivity implements View.OnClickListener {
    public TripInterestUtils tripInterestUtils;

    public static Trip selectedTrip;
    public Bitmap tripPhoto;
    private ImageView tripAddPhoto;
    private Utils utils;
    private LinearLayout tripSubmitLoading;
    private TextView tripSubmitLoadingText;
    private ChipGroup tripInterestChipGroup;
    private PhotoUploader uploader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_create_activity);
        utils = new Utils(this);
        uploader = new PhotoUploader(this);
        tripInterestUtils = new TripInterestUtils(this, selectedTrip.interests, selectedTrip.idInt);

        tripAddPhoto = findViewById(R.id.trip_add_photo);
        tripAddPhoto.setOnClickListener(this);
        findViewById(R.id.trip_add_btn_done).setOnClickListener(this);
        tripSubmitLoading = findViewById(R.id.trip_add_loading);
        tripSubmitLoadingText = findViewById(R.id.trip_add_loading_text);
        tripInterestChipGroup = findViewById(R.id.interests_chip_group);

        tripInterestUtils.setViewGroup(tripInterestChipGroup, true);
        tripInterestUtils.updateView();

        setupDatePicker((EditText) findViewById(R.id.trip_add_start), findViewById(R.id.trip_add_start_select));
        setupDatePicker((EditText) findViewById(R.id.trip_add_end), findViewById(R.id.trip_add_end_select));

        ((TextView) findViewById(R.id.trip_create_activity_title)).setText("Edit trip");

        populateDataIntoView();
    }

    private void populateDataIntoView() {
        findViewById(R.id.trip_add_btn_add_step).setVisibility(View.GONE);
        ((EditText) findViewById(R.id.trip_add_title)).setText(selectedTrip.title);
        ((EditText) findViewById(R.id.trip_add_desc)).setText(selectedTrip.description);
        ((EditText) findViewById(R.id.trip_add_start)).setText(selectedTrip.start.format(Utils.DATEFORMATTER));
        ((EditText) findViewById(R.id.trip_add_end)).setText(selectedTrip.end.format(Utils.DATEFORMATTER));

        if (selectedTrip.picture_url != null) {
            Picasso.get().load(selectedTrip.picture_url).into(tripAddPhoto);
        } else {
            tripAddPhoto.setImageResource(R.drawable.bg_upload_placeholder);
        }
    }

    private String getTripInputTitle() {
        return ((EditText) findViewById(R.id.trip_add_title)).getText().toString();
    }

    private String getTripInputDesc() {
        return ((EditText) findViewById(R.id.trip_add_desc)).getText().toString();
    }

    public void showPhotoSelector() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_TRIP_PHOTO);
    }

    /**
     * SUBMIT CODE
     */
    private void submit() {
        String startDate = ((EditText) findViewById(R.id.trip_add_start)).getText().toString();
        String endDate = ((EditText) findViewById(R.id.trip_add_end)).getText().toString();

        if (isEmpty("Trip title", getTripInputTitle())) return;
        if (isEmpty("Trip start date", startDate)) return;
        if (isEmpty("Trip end date", endDate)) return;

        final HashMap<String, Object> params = new HashMap<>();
        params.put("close", false);
        params.put("title", getTripInputTitle());
        params.put("description", getTripInputDesc());
        params.put("start", Utils.toISODate(startDate));
        params.put("end", Utils.toISODate(endDate));

        tripSubmitLoading.setVisibility(View.VISIBLE);
        tripSubmitLoadingText.setText("Please wait...");
        utils.callAPI("PUT", "/voyages/" + selectedTrip.id, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JsonObject obj = Utils.toGSON(response);
                    tripSubmitLoadingText.setText("Saving photo...");
                    Trip nTrip = new Trip(obj);
                    uploadPhoto(nTrip);
                } catch (Exception e) {
                    tripSubmitLoading.setVisibility(View.GONE);
                    Utils.toast(TripEditActivity.this, "ERROR! Please try again later");
                }
            }
        });
    }

    private void uploadPhoto(final Trip trip) {
        final Response.Listener<JSONObject> callback = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JsonObject obj = Utils.toGSON(response);
                    Trip.removeCache(trip.id);
                    Trip.get(obj);
                    onSubmissionDone();
                } catch (Exception e) {
                    onSubmissionDone();
                }
            }
        };

        if (tripPhoto == null) {
            utils.callAPI("GET", "/voyages/" + trip.id, null, callback);
            return;
        }

        Multipart m1 = new Multipart(this);
        Map<String, String> p1 = new HashMap<>();
        m1.addFile("image/jpeg", "files", "image.jpg",
                Utils.getFileDataFromBitmap(tripPhoto));
        p1.put("refId", trip.id);
        p1.put("ref", "voyage");
        p1.put("field", "picture");
        m1.addParams(p1);

        uploader.upload(m1, new AsyncResponse<JsonObject>() {
            @Override
            public void run() {
                utils.callAPI("GET", "/voyages/" + trip.id, null, callback);
            }
        });
    }

    private void onSubmissionDone() {
        ProfileFragment.shouldReload = true;
        DiscoveryFragment.shouldReload = true;
        Utils.toast(TripEditActivity.this, "Success!");
        Intent returnIntent = new Intent();
        setResult(RESULT_OK, returnIntent);
        finish();
    }

    private boolean isEmpty(String name, String content) {
        if (content == null || content.trim().length() == 0) {
            Utils.toast(this, name + " cannot be empty!");
            return true;
        } else {
            return false;
        }
    }

    /**
     * END OF SUBMIT CODE
     */

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.trip_add_photo:
                showPhotoSelector();
                break;

            case R.id.trip_add_btn_done:
                submit();
                break;
        }
    }

    @Override
    public void onBackPressed() {
        Intent returnIntent = new Intent();
        setResult(RESULT_CANCELED, returnIntent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.REQUEST_CODE_SELECT_TRIP_PHOTO && resultCode == RESULT_OK && data != null) {
            try {
                Uri path = data.getData();
                tripPhoto = MediaStore.Images.Media.getBitmap(getContentResolver(), path);
                tripAddPhoto.setImageBitmap(tripPhoto);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setupDatePicker(final EditText inputView, View btn) {
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar mcurrentDate = Calendar.getInstance();
                int mYear = mcurrentDate.get(Calendar.YEAR);
                int mMonth = mcurrentDate.get(Calendar.MONTH);
                int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog mDatePicker = new DatePickerDialog(TripEditActivity.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int yyyy, int mm0, int dd) {
                        int mm = mm0 + 1;
                        inputView.setText((dd < 10 ? "0" : "") + dd + "/"
                                + (mm < 10 ? "0" : "") + mm + "/" + yyyy);
                    }
                }, mYear, mMonth, mDay);
                mDatePicker.setTitle("Select date");
                mDatePicker.show();
            }
        });
    }
}
