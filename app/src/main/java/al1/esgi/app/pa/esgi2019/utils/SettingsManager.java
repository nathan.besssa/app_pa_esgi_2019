package al1.esgi.app.pa.esgi2019.utils;

import al1.esgi.app.pa.esgi2019.model.User;
import android.content.Context;
import android.content.SharedPreferences;
import com.android.volley.Response;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

public class SettingsManager {
    public static int currentUserId = -1;
    public static User currentUser;
    private final String MY_PREFS_NAME = "app_settings";
    private Context ctx;
    private SharedPreferences prefs;

    public SettingsManager(Context ctx) {
        this.ctx = ctx;
        this.prefs = ctx.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        getUid();
    }

    public String getToken() {
        return prefs.getString("token", "none");
    }

    public int getUid() {
        currentUserId = prefs.getInt("uid", -1);
        return currentUserId;
    }

    public void saveAuth(String token, int id) {
        currentUserId = id;
        prefs.edit()
                .putString("token", token)
                .putInt("uid", id).apply();
    }

    public void reloadCurrentUser(final AsyncResponse<User> callback) {
        Utils utils = new Utils(ctx);
        utils.callAPI("GET", "/users/me", new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                currentUser = new User(Utils.toGSON(response));
                if (callback == null) return;
                callback.responseData = currentUser;
                callback.run();
            }
        });
    }

    public void clearAuth() {
        prefs.edit().remove("token").remove("uid").apply();
    }
}
