package al1.esgi.app.pa.esgi2019.trip;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.adapter.StepListAdapter;
import al1.esgi.app.pa.esgi2019.adapter.TripCardAdapter;
import al1.esgi.app.pa.esgi2019.home.DiscoveryFragment;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.profile.ProfileFragment;
import al1.esgi.app.pa.esgi2019.step.StepEditActivity;
import al1.esgi.app.pa.esgi2019.utils.NonScrollListView;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.Response;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import java.util.ArrayList;

public class TripActivity extends AppCompatActivity {
    public TripInterestUtils tripInterestUtils;

    private RelativeLayout tripContentView;
    private NonScrollListView tripStepsListView;
    private Trip trip;
    private StepListAdapter adapter;
    private TextView btnAddStep;
    private String tripId;
    private ChipGroup tripInterestChipGroup;
    private Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_activity);
        utils = new Utils(this);

        tripContentView = findViewById(R.id.trip_content_view);
        tripStepsListView = findViewById(R.id.trip_steps);
        btnAddStep = findViewById(R.id.trip_add_btn_add_step);
        tripInterestChipGroup = findViewById(R.id.interests_chip_group);
        ImageButton btnMenu = findViewById(R.id.trip_btn_menu);

        tripId = getIntent().getStringExtra("id");
        trip = Trip.get(tripId);
        renderTripMeta(trip);
        renderTripSteps(trip);

        Utils.scrollToTop(this, R.id.main_view);
        if (!trip.isSelfCreated) btnMenu.setVisibility(View.GONE);
        btnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String[] colors = {"Modify", "Delete", "Add step"};

                AlertDialog.Builder builder = new AlertDialog.Builder(TripActivity.this);
                builder.setTitle(trip.title);
                builder.setItems(colors, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (which == 0) {
                            Intent intent = new Intent(TripActivity.this, TripEditActivity.class);
                            TripEditActivity.selectedTrip = trip;
                            startActivityForResult(intent, 1);
                        } else if (which == 1) {
                            askDelete();
                        } else if (which == 2) {
                            onClickAddStep();
                        }
                    }
                });
                builder.show();
            }
        });
    }

    private void askDelete() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        Log.d("callAPI", "/voyages/" + trip.id);
                        utils.callAPI("DELETE", "/voyages/" + trip.id, null, new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                Utils.toast(TripActivity.this, "Trip deleted");
                                ProfileFragment.shouldReload = true;
                                DiscoveryFragment.shouldReload = true;
                                finish();
                            }
                        });
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Do you really want to delete this trip?")
                .setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void renderTripMeta(Trip trip) {
        ArrayList<Trip> trips = new ArrayList<>();
        trips.add(trip);
        final TripCardAdapter adapter = new TripCardAdapter(trips, this);
        adapter.setLayout(R.layout.discovery_card, true, false);
        View v = adapter.getView(0, null, tripContentView);
        tripInterestUtils = new TripInterestUtils(this, trip.interests, trip.idInt);
        tripInterestUtils.setViewGroup(tripInterestChipGroup, false);
        tripInterestUtils.updateView();
        tripContentView.removeAllViews();
        tripContentView.addView(v);
    }

    private void renderTripSteps(Trip trip) {
        adapter = new StepListAdapter(this, R.layout.trip_step_item, trip.steps);
        adapter.ac = this;
        tripStepsListView.setAdapter(adapter);

        btnAddStep.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickAddStep();
            }
        });
        if (trip.isSelfCreated) btnAddStep.setVisibility(View.VISIBLE);
    }

    private void onClickAddStep() {
        Intent intent = new Intent(TripActivity.this, StepEditActivity.class);
        StepEditActivity.trip = trip;
        StepEditActivity.step = null;
        startActivityForResult(intent, 1);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        final Response.Listener<JSONObject> callback = new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JsonObject obj = Utils.toGSON(response);
                    Trip.removeCache(trip.id);
                    trip = Trip.get(obj);
                    renderTripMeta(trip);
                } catch (Exception e) {}
            }
        };

        utils.callAPI("GET", "/voyages/" + trip.id, null, callback);
        adapter.notifyDataSetInvalidated();
    }
}
