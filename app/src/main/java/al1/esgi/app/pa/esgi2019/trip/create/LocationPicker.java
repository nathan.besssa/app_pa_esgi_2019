package al1.esgi.app.pa.esgi2019.trip.create;

import al1.esgi.app.pa.esgi2019.R;
import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class LocationPicker extends AppCompatActivity implements OnMapReadyCallback {

    GoogleMap mGoogleMap;
    double mLat = 0;
    double mLng = 0;
    String mFullAddress = "";
    Marker marker;
    Button btnSelectLocation;

    private void goToLocationZoom(double lat, double lng, float zoom) {
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, zoom);
        mGoogleMap.moveCamera(update);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.trip_create_map_activity);

        btnSelectLocation = findViewById(R.id.valid_location);
        btnSelectLocation.setVisibility(View.GONE);

        if (googleServicesAvailable()) {
            Toast.makeText(this, "Please enter the place!", Toast.LENGTH_LONG).show();

            initMap();
            validatePosition();
        } else {
            // No Google Maps Layout
        }
    }

    private void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }

    public boolean googleServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {
            Dialog dialog = api.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Cant connect to play services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        if (mGoogleMap != null) {
            mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                private double locationLat;

                @Override
                public View getInfoWindow(Marker marker) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    View v = getLayoutInflater().inflate(R.layout.trip_create_map_info, null);

                    TextView locality = v.findViewById(R.id.locality);
                    TextView lat = v.findViewById(R.id.lat);
                    TextView lon = v.findViewById(R.id.lon);
                    TextView snippet = v.findViewById(R.id.snippet);

                    LatLng ll = marker.getPosition();
                    locality.setText(marker.getTitle());
                    lat.setText("Latitude: " + ll.latitude);
                    this.locationLat = ll.latitude;
                    Log.i("Lat: ", " " + ll.latitude);
                    lon.setText("Longitude: " + ll.longitude);
                    Log.i("Long: ", " " + ll.longitude);
                    snippet.setText(marker.getSnippet());

                    return v;
                }
            });
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    Activity#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for Activity#requestPermissions for more details.
                return;
            }
        }
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
    }

    public void geoLocate(View view) {
        EditText et = findViewById(R.id.search_text);
        String location = et.getText().toString();
        if (location.trim().equals("")) {
            Toast.makeText(this, "Please enter an invalid place", Toast.LENGTH_LONG).show();
        }

        Geocoder gc = new Geocoder(this);

        try {
            List<Address> list = gc.getFromLocationName(location, 1);
            Address address = list.get(0);

            double lat = address.getLatitude();
            double lng = address.getLongitude();
            goToLocationZoom(lat, lng, 15);

            String full_address = "";
            full_address += addrField(address.getFeatureName(), ", ");
            full_address += addrField(address.getLocality(), ", ");
            full_address += addrField(address.getCountryName(), "");
            Toast.makeText(this, full_address, Toast.LENGTH_SHORT).show();
            setMarker(full_address, lat, lng);

            mLat = lat;
            mLng = lng;
            mFullAddress = full_address == null ? "" : full_address;

            btnSelectLocation.setVisibility(View.VISIBLE);
        } catch (Exception e) {
            Toast.makeText(this, "Cannot find this location", Toast.LENGTH_SHORT).show();
            btnSelectLocation.setVisibility(View.GONE);
        }

    }

    private String addrField(String s, String end) {
        return (s != null && s.trim().length() > 0 ? s + end : "");
    }

    private void setMarker(String locality, double lat, double lng) {
        if (marker != null) {
            marker.remove();
        }

        MarkerOptions options = new MarkerOptions()
                .title(locality)
                .position(new LatLng(lat, lng))
                .snippet("I travel here");
        marker = mGoogleMap.addMarker(options);
    }

    public void validatePosition() {
        btnSelectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("lat", mLat);
                returnIntent.putExtra("lng", mLng);
                returnIntent.putExtra("full_address", mFullAddress);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

}
