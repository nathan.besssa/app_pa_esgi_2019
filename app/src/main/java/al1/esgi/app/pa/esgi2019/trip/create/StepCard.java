package al1.esgi.app.pa.esgi2019.trip.create;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.trip.TripStepData;
import al1.esgi.app.pa.esgi2019.utils.AsyncResponse;
import al1.esgi.app.pa.esgi2019.utils.Constants;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import com.android.volley.Response;
import com.google.gson.JsonObject;
import org.json.JSONObject;

import java.util.HashMap;

public class StepCard {
    TripCreateActivity ac;
    Utils utils;

    public StepCard(final TripCreateActivity ac) {
        this.ac = ac;
        utils = new Utils(ac);
    }

    public void createCard() {
        final TripStepData v = (TripStepData) ac.getLayoutInflater().inflate(R.layout.trip_create_step_item, null);
        ac.setupDatePicker((EditText) v.findViewById(R.id.step_start), v.findViewById(R.id.step_start_select));
        ac.tripStepList.addView(v);
        ac.stepViews.add(v);

        v.findViewById(R.id.step_btn_delete).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac.tripStepList.removeView(v);
                ac.stepViews.remove(v);
            }
        });

        v.findViewById(R.id.step_image).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPhotoSelector(v);
            }
        });

        v.findViewById(R.id.step_btn_pick_location).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ac.showLocationPicker(v);
            }
        });
    }

    public void updateLocation(Intent intent) {
        TripStepData v = ac.selectedStepForLocationPicker;
        v.lat = intent.getDoubleExtra("lat", 0);
        v.lng = intent.getDoubleExtra("lng", 0);
        EditText addr = v.findViewById(R.id.step_adress);
        addr.setText(intent.getStringExtra("full_address"));
    }

    public void showPhotoSelector(TripStepData v) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        ac.currentStepData = v;
        ac.startActivityForResult(intent, Constants.REQUEST_CODE_SELECT_STEP_PHOTO);
    }

    public boolean hasEmptyCard() {
        for (TripStepData cardView : ac.stepViews) {
            if (isEmpty("Step title", ((EditText) cardView.findViewById(R.id.step_title)).getText().toString()))
                return true;
            if (isEmpty("Step start date", ((EditText) cardView.findViewById(R.id.step_start)).getText().toString()))
                return true;
            if (isEmpty("Step address", ((EditText) cardView.findViewById(R.id.step_adress)).getText().toString()))
                return true;
        }
        return false;
    }

    public void runUploadSteps(int tripId, AsyncResponse<Boolean> callback) {
        recursiveUploadSteps(tripId, 0, callback);
    }

    public void recursiveUploadSteps(final int tripId, final int position, final AsyncResponse<Boolean> callback) {
        if (position == ac.stepViews.size()) {
            callback.run();
            return;
        }

        final TripStepData cardView = ac.stepViews.get(position);
        String startDate = ((EditText) cardView.findViewById(R.id.step_start)).getText().toString();
        final HashMap<String, Object> params = new HashMap<>();
        params.put("adress", ((EditText) cardView.findViewById(R.id.step_adress)).getText().toString());
        params.put("comment", ((EditText) cardView.findViewById(R.id.step_comment)).getText().toString());
        params.put("mark", (int) ((RatingBar) cardView.findViewById(R.id.step_mark)).getRating());
        params.put("start", Utils.toISODate(startDate));
        params.put("title", ((EditText) cardView.findViewById(R.id.step_title)).getText().toString());
        params.put("voyage", tripId);

        utils.callAPI("POST", "/etapes", new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JsonObject obj = Utils.toGSON(response);
                    cardView.stepId = obj.get("id").getAsInt();
                    Log.d("trip", "saved step = " + cardView.stepId);
                } catch (Exception e) {
                }
                recursiveUploadSteps(tripId, position + 1, callback);
            }
        });
    }

    private boolean isEmpty(String name, String content) {
        if (content == null || content.trim().length() == 0) {
            Utils.toast(ac, name + " cannot be empty!");
            return true;
        } else {
            return false;
        }
    }
}
