package al1.esgi.app.pa.esgi2019.auth;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Response;
import org.json.JSONObject;

import java.util.HashMap;

public class SignUpActivity extends AppCompatActivity {
    String username = "";
    String password = "";
    Button btn_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);

        btn_register = findViewById(R.id.btn_register);
        btn_register.setText("Continue");
        btn_register.setEnabled(true);
        btn_register.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
               register(getParams());
            }
        });
    }

    public void register(HashMap<String, String> params){
        btn_register.setText("Please wait...");
        btn_register.setEnabled(false);

        Utils utils = new Utils(this);
        utils.callAPI("POST", "/auth/local/register", new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response == null) {
                    btn_register.setText("Continue");
                    btn_register.setEnabled(true);
                    Toast.makeText(SignUpActivity.this,
                            "Error, cannot create new account",
                            Toast.LENGTH_SHORT).show();
                } else {
                    success();
                }
            }
        });
    }

    private void success() {
        Toast.makeText(this, "Created new account! Please login to continue",
                Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LogInActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);
        intent.putExtra("username", username);
        intent.putExtra("password", password);
        startActivity(intent);
        finish();
    }

    private HashMap<String, String> getParams(){
        HashMap<String, String> params = new HashMap<>();

        EditText input_firstname = findViewById(R.id.input_firstname);
        EditText input_lastname = findViewById(R.id.input_lastname);
        EditText input_username = findViewById(R.id.input_username);
        EditText input_email_register = findViewById(R.id.input_email_register);
        EditText input_password_register = findViewById(R.id.input_password_register);


        username = input_username.getText().toString().trim();
        password = input_password_register.getText().toString().trim();

        params.put("lastname", input_lastname.getText().toString().trim());
        params.put("firstname", input_firstname.getText().toString().trim());
        params.put("username", input_username.getText().toString().trim());
        params.put("email", input_email_register.getText().toString().trim());
        params.put("password", input_password_register.getText().toString().trim());

        return params;
    }
}



