package al1.esgi.app.pa.esgi2019.adapter;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.profile.ProfileActivity;
import al1.esgi.app.pa.esgi2019.trip.TripActivity;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.model.User;
import al1.esgi.app.pa.esgi2019.utils.Constants;
import al1.esgi.app.pa.esgi2019.utils.SettingsManager;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.android.volley.Response;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class TripCardAdapter extends BaseAdapter {
    private List<Trip> data;
    private Context context;
    private int layoutId;
    private boolean showUserTopBar;
    private SettingsManager settingsManager;
    private boolean isLoading;
    private boolean isContentClickable;

    public TripCardAdapter(List<Trip> data, Context context) {
        this.data = data;
        this.context = context;
        this.settingsManager = new SettingsManager(context);
    }

    public void setLayout(int id, boolean showUserTopBar) {
        this.layoutId = id;
        this.showUserTopBar = showUserTopBar;
    }

    public void setLayout(int id, boolean showUserTopBar, boolean isContentClickable) {
        setLayout(id, showUserTopBar);
        this.isContentClickable = isContentClickable;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(layoutId, parent, false);
        }

        Trip trip = data.get(position);
        return renderView(trip, v);
    }

    public View renderView(final Trip trip, final View v) {
        TextView tripTitle = v.findViewById(R.id.item_trip_title);
        TextView tripDesc = v.findViewById(R.id.item_trip_description);
        ImageView tripImage = v.findViewById(R.id.item_trip_image);
        TextView tvLikesCount = v.findViewById(R.id.tv_like_count);
        ImageView btnLike = v.findViewById(R.id.btn_like);
        tripTitle.setText(trip.title);

        if (isContentClickable) {
            tripDesc.setText(trip.description);
        } else {
            tripDesc.setText(
                    "Start: " + trip.startDateString + "\n"
                            + "End: " + trip.endDateString + "\n"
                            + "\n" + trip.description
            );
        }

        if (trip.picture_url != null) {
            Picasso.get().load(trip.picture_url).into(tripImage);
        } else {
            tripImage.setImageResource(R.drawable.bg_img_placeholder);
        }

        View.OnClickListener onClickCard = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, TripActivity.class);
                intent.putExtra("id", trip.id);
                ((Activity) context).startActivityForResult(intent, Constants.REQUEST_CODE_VIEW_TRIP);
            }
        };
        if (isContentClickable) v.setOnClickListener(onClickCard);


        tvLikesCount.setText("" + trip.likesCount);
        btnLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    onClickLikeBtn(trip, v);
                } catch (JSONException e) {
                }
            }
        });


        btnLike.setImageResource(trip.isSelfLiked
                ? R.drawable.ic_heart_filled
                : R.drawable.ic_heart_outline);

        if (showUserTopBar) {
            ImageView authorAvatar = v.findViewById(R.id.item_trip_author_avatar);
            TextView authorName = v.findViewById(R.id.item_trip_author_fullname);
            TextView authorUserName = v.findViewById(R.id.item_trip_author_username);

            try {
                final User author = trip.getUser();
                Picasso.get().load(author.picture_url).into(authorAvatar);
                authorName.setText(author.fullname);
                authorUserName.setText("@" + author.username);
                View.OnClickListener onClickAuthor = new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (trip.isSelfCreated) return;
                        Intent intent = new Intent(context, ProfileActivity.class);
                        intent.putExtra("id", author.id);
                        ((Activity) context).startActivityForResult(intent, Constants.REQUEST_CODE_VIEW_TRIP);
                    }
                };
                authorAvatar.setOnClickListener(onClickAuthor);
                authorName.setOnClickListener(onClickAuthor);
                authorUserName.setOnClickListener(onClickAuthor);
            } catch (Exception e) {
                Log.e("usernull", "trip id = " + trip.id);
            }
        }

        return v;
    }

    private void onClickLikeBtn(final Trip trip, final View v) throws JSONException {
        if (isLoading) return;
        isLoading = true;
        Utils utils = new Utils(context);
        JSONArray likedPosts = new JSONArray();

        // build new user.likes without current post id
        int tripId = Integer.parseInt(trip.id);
        for (int likedTripId : SettingsManager.currentUser.likedPostIds) {
            if (likedTripId != tripId) {
                likedPosts.put(likedTripId);
            }
        }

        // add current post to user.likes if we haven't liked the post yet
        if (!trip.isSelfLiked) {
            likedPosts.put(tripId);
        }

        JSONObject obj = new JSONObject();
        obj.put("likes", likedPosts);
        utils.callAPI("PUT", "/users/" + SettingsManager.currentUserId, obj, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                isLoading = false;
                trip.toggleLike(SettingsManager.currentUser);
                settingsManager.reloadCurrentUser(null);
                renderView(trip, v);
            }
        });
    }
}
