package al1.esgi.app.pa.esgi2019.profile;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.search.SearchActivity;
import al1.esgi.app.pa.esgi2019.adapter.TripCardAdapter;
import al1.esgi.app.pa.esgi2019.model.Interest;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.model.User;
import al1.esgi.app.pa.esgi2019.utils.EncodingUtil;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.util.Log;
import android.view.View;
import android.widget.*;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import com.android.volley.Response;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.ArrayList;

public class ProfileLikeTripsActivity extends AppCompatActivity {
    public static String userId = "";
    public static String userName = "";

    private Utils utils;
    private ProgressBar loading;
    private TextView notFound;
    private ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_like_trips_activity);

        utils = new Utils(this);
        loading = findViewById(R.id.loading);
        notFound = findViewById(R.id.text_not_found);
        listView = findViewById(R.id.listview_trips);

        loading.setVisibility(View.GONE);
        notFound.setVisibility(View.GONE);

        ((TextView) findViewById(R.id.profile_like_trips_desc)).setText("Trips liked by " + userName);

        fetchData();
    }

    private void fetchData() {
        loading.setVisibility(View.VISIBLE);
        notFound.setVisibility(View.GONE);
        utils.callAPIArray("GET", "/users?id=" + userId, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                ArrayList<Trip> trips = new ArrayList<>();
                try {
                    JsonObject user = Utils.toGSON(response.getJSONObject(0));
                    JsonArray likes = user.get("likes").getAsJsonArray();
                    for (JsonElement el : likes) {
                        trips.add(Trip.get(el.getAsJsonObject()));
                    }
                    if (trips.size() == 0) {
                        notFound.setVisibility(View.VISIBLE);
                    }
                    loading.setVisibility(View.GONE);
                    final TripCardAdapter adapter = new TripCardAdapter(trips, ProfileLikeTripsActivity.this);
                    adapter.setLayout(R.layout.profile_trip_card, false, true);
                    listView.setAdapter(adapter);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
