package al1.esgi.app.pa.esgi2019.auth;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.home.HomeActivity;
import al1.esgi.app.pa.esgi2019.utils.Constants;
import al1.esgi.app.pa.esgi2019.utils.MySingleton;
import al1.esgi.app.pa.esgi2019.utils.SettingsManager;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.json.JSONObject;

import java.util.HashMap;

public class LogInActivity extends AppCompatActivity {
    private Button login_submit;
    private EditText username_login;
    private EditText password_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.login_activity);

        login_submit = findViewById(R.id.login_submit);
        login_submit.setText("Login");
        login_submit.setEnabled(true);
        login_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                login(getParams());
            }
        });

        TextView link_signup = findViewById(R.id.link_signup);
        link_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                goToSignUp();
            }
        });

        username_login = findViewById(R.id.username_login);
        password_login = findViewById(R.id.password_login);
        String prefillUsername = getIntent().getStringExtra("username");
        String prefillPassword = getIntent().getStringExtra("password");
        if (prefillUsername != null) username_login.setText(prefillUsername);
        if (prefillPassword != null) password_login.setText(prefillPassword);
    }

    public void goToSignUp() {
        Utils.openActivity(this, SignUpActivity.class);
    }

    public void login(HashMap<String, String> params) {
        login_submit.setText("Please wait...");
        login_submit.setEnabled(false);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
                Request.Method.POST,
                Constants.api("/auth/local"),
                new JSONObject(params),
                new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        String res = response.toString();
                        Log.d("auth", res);
                        saveToken(res);
                        Utils.clearAndOpenActivity(LogInActivity.this, HomeActivity.class);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("error", "error: " + error.getMessage());
                login_submit.setText("Login");
                login_submit.setEnabled(true);
                Toast.makeText(
                        getApplicationContext(),
                        "Incorrect username or password",
                        Toast.LENGTH_SHORT
                ).show();
            }
        }
        );
        MySingleton.getInstance(this).addToRequestQueue(jsonObjectRequest);
    }

    private void saveToken(String logInResponse) {
        JsonObject jsonObject = new JsonParser().parse(logInResponse).getAsJsonObject();
        String jwt_token = jsonObject.get("jwt").getAsString();
        jsonObject = jsonObject.getAsJsonObject("user");
        int id = jsonObject.get("id").getAsInt();
        SettingsManager settings = new SettingsManager(this);
        settings.saveAuth(jwt_token, id);
    }

    private HashMap<String, String> getParams() {
        HashMap<String, String> params = new HashMap<>();

        params.put("identifier", username_login.getText().toString().trim());
        params.put("password", password_login.getText().toString().trim());

        return params;
    }

}
