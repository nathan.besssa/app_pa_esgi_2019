package al1.esgi.app.pa.esgi2019.model;

import com.google.gson.JsonObject;

public class Interest {
    public int id;
    public String tag;
    public int userId;
    public Trip trip;

    public Interest(String tag) {
        this.tag = tag;
    }

    public Interest(JsonObject obj) {
        id = obj.get("id").getAsInt();
        tag = obj.get("tag").getAsString();
        try {
            userId = obj.get("user").getAsInt();
        } catch (Exception e) {}
        try {
            trip = Trip.get(obj.get("voyage").getAsJsonObject());
        } catch (Exception e) {}
    }
}
