package al1.esgi.app.pa.esgi2019.profile;

import al1.esgi.app.pa.esgi2019.MainActivity;
import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.model.Interest;
import al1.esgi.app.pa.esgi2019.model.User;
import al1.esgi.app.pa.esgi2019.utils.SettingsManager;
import al1.esgi.app.pa.esgi2019.utils.Utils;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import com.android.volley.Response;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import org.json.JSONObject;

import java.util.HashMap;

public class ProfileEditActivity extends AppCompatActivity {
    private EditText inputFirstName;
    private EditText inputLastName;
    private EditText inputEmail;
    private EditText inputPhone;
    private EditText inputAddInterest;
    private EditText inputPassword;
    private EditText inputPasswordConfirm;
    private Utils utils;
    private SettingsManager settings;
    private ChipGroup interestChipGroup;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_edit_activity);
        utils = new Utils(this);
        settings = new SettingsManager(this);

        inputFirstName = findViewById(R.id.input_firstname);
        inputLastName = findViewById(R.id.input_lastname);
        inputEmail = findViewById(R.id.input_email);
        inputPhone = findViewById(R.id.input_phone);
        inputAddInterest = findViewById(R.id.input_add_interest);
        inputPassword = findViewById(R.id.input_password);
        inputPasswordConfirm = findViewById(R.id.input_password_again);
        user = SettingsManager.currentUser;

        if (user == null) {
            Utils.replaceActivity(this, MainActivity.class);
            return;
        }

        inputFirstName.setText(user.firstname);
        inputLastName.setText(user.lastname);
        inputEmail.setText(user.email);
        inputPhone.setText(user.phone);

        findViewById(R.id.btn_update_profile).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });
        findViewById(R.id.btn_change_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changePassword();
            }
        });
        findViewById(R.id.btn_add_interest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addInterestChip();
            }
        });

        interestChipGroup = findViewById(R.id.interests_chip_group);
        for (Interest interest : user.interests) {
            interestChipGroup.addView(getInterestChip(interest));
        }
    }

    private Chip getInterestChip(final Interest interest) {
        final Chip chip = new Chip(this);
        int paddingDp = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 10,
                getResources().getDisplayMetrics()
        );
        chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp);
        chip.setText(interest.tag);
        chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askRemoveChip(chip, interest);
            }
        });
        chip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askRemoveChip(chip, interest);
            }
        });
        chip.setClickable(true);
        chip.setCloseIconVisible(true);
        return chip;
    }

    private void askRemoveChip(final Chip chip, final Interest interest) {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        removeChip(chip, interest);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Confirm delete " + interest.tag + "?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    private void removeChip(final Chip chip, final Interest interest) {
        utils.callAPI("DELETE", "/interests/" + interest.id, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    interestChipGroup.removeView(chip);
                } catch (Exception e) {
                }
            }
        });
    }

    private void addInterestChip() {
        ProfileFragment.shouldReload = true;
        final HashMap<String, Object> params = new HashMap<>();
        params.put("tag", inputAddInterest.getText().toString());
        params.put("user", user.id);

        inputAddInterest.setText("");

        utils.callAPI("POST", "/interests", new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Interest in = new Interest(Utils.toGSON(response));
                in.userId = Integer.parseInt(user.id);
                interestChipGroup.addView(getInterestChip(in));
            }
        });
    }

    private void updateProfile() {
        final HashMap<String, Object> params = new HashMap<>();
        params.put("firstname", inputFirstName.getText().toString());
        params.put("lastname", inputLastName.getText().toString());
        params.put("email", inputEmail.getText().toString());
        params.put("phone", inputPhone.getText().toString());

        utils.callAPI("PUT", "/users/" + settings.getUid(), new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response == null) {
                    Toast.makeText(ProfileEditActivity.this, "Cannot update profile", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileEditActivity.this, "Updated profile", Toast.LENGTH_SHORT).show();
                    ProfileFragment.shouldReload = true;
                    ProfileEditActivity.this.finish();
                }
            }
        });
    }

    private void changePassword() {
        if (!inputPassword.getText().toString().equals(inputPasswordConfirm.getText().toString())) {
            Toast.makeText(this, "Confirmation does not match", Toast.LENGTH_SHORT).show();
        }

        final HashMap<String, Object> params = new HashMap<>();
        params.put("password", inputPassword.getText().toString());

        utils.callAPI("PUT", "/users/" + settings.getUid(), new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                if (response == null) {
                    Toast.makeText(ProfileEditActivity.this, "Cannot update profile", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(ProfileEditActivity.this, "Updated profile", Toast.LENGTH_SHORT).show();
                    ProfileEditActivity.this.finish();
                }
            }
        });
    }
}
