package al1.esgi.app.pa.esgi2019.profile;

import al1.esgi.app.pa.esgi2019.R;
import al1.esgi.app.pa.esgi2019.WelcomeActivity;
import al1.esgi.app.pa.esgi2019.home.DiscoveryUtils;
import al1.esgi.app.pa.esgi2019.trip.create.TripCreateActivity;
import al1.esgi.app.pa.esgi2019.adapter.TripCardAdapter;
import al1.esgi.app.pa.esgi2019.model.Interest;
import al1.esgi.app.pa.esgi2019.model.Trip;
import al1.esgi.app.pa.esgi2019.model.User;
import al1.esgi.app.pa.esgi2019.utils.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import com.android.volley.Response;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;

import static android.app.Activity.RESULT_OK;

public class ProfileFragment extends Fragment {
    public static boolean shouldReload = false;
    public Button btnFollow;
    public Button btnFollowed;
    private OnFragmentProfileListener mListener;
    private SettingsManager settings;
    private Utils utils;
    private View mainView;
    private User user;
    private DiscoveryUtils dcUtils;
    private PhotoUploader photoUploader;
    private ChipGroup interestChipGroup;
    private String profileId = "me";
    private NonScrollListView tripsList;
    private ProfileFollowUtils profileFollowUtils;

    public ProfileFragment(Context ctx) {
        settings = new SettingsManager(ctx);
        utils = new Utils(ctx);
        dcUtils = new DiscoveryUtils(ctx);
        photoUploader = new PhotoUploader(ctx);
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.profile_fragment, container, false);
        Button btnLogout = mainView.findViewById(R.id.btn_logout);
        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askLogout();
            }
        });
        Button btnEdit = mainView.findViewById(R.id.btn_edit_profile);
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utils.openActivity(getActivity(), ProfileEditActivity.class);
            }
        });
        Button btnAddTrip = mainView.findViewById(R.id.btn_add_trip);
        btnAddTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), TripCreateActivity.class);
                getActivity().startActivityForResult(intent, Constants.REQUEST_CODE_NEW_TRIP);
            }
        });

        btnFollow = mainView.findViewById(R.id.btn_follow);
        btnFollowed = mainView.findViewById(R.id.btn_followed);
        btnFollow.setVisibility(View.GONE);
        btnFollowed.setVisibility(View.GONE);

        View.OnClickListener onChangeProfile = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                photoUploader.showPhotoSelector(getActivity());
            }
        };
        mainView.findViewById(R.id.avatar).setOnClickListener(onChangeProfile);
        TextView btnChangePhoto = mainView.findViewById(R.id.btn_change_photo);
        btnChangePhoto.setOnClickListener(onChangeProfile);
        interestChipGroup = mainView.findViewById(R.id.interests_chip_group);

        if (!isSelfProfile()) {
            btnLogout.setVisibility(View.GONE);
            btnEdit.setVisibility(View.GONE);
            btnAddTrip.setVisibility(View.GONE);
            btnChangePhoto.setVisibility(View.GONE);
        }

        updateProfile();

        return mainView;
    }

    private void updateProfile() {
        if (isSelfProfile()) {
            settings.reloadCurrentUser(new AsyncResponse<User>() {
                @Override
                public void run() {
                    renderView(this.responseData);
                }
            });
        } else {
            utils.callAPIArray("GET", "/users?id=" + profileId, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {
                    try {
                        user = new User(Utils.toGSON(response.getJSONObject(0)));
                        renderView(user);
                        btnFollow.setVisibility(View.VISIBLE);
                        profileFollowUtils = new ProfileFollowUtils(ProfileFragment.this);
                        profileFollowUtils.renderFollowButton(user);
                    } catch (JSONException e) {
                        Toast.makeText(getContext(), "Cannot load profile", Toast.LENGTH_SHORT).show();
                    }
                }
            });

        }

        dcUtils.getUserTrips(new AsyncResponse<ArrayList<Trip>>() {
            @Override
            public void run() {
                final TripCardAdapter adapter = new TripCardAdapter(
                        this.responseData, ProfileFragment.this.getContext());
                adapter.setLayout(R.layout.profile_trip_card, false, true);
                tripsList = mainView.findViewById(R.id.listview_trips);
                tripsList.setAdapter(adapter);
            }
        }, isSelfProfile() ? settings.getUid() : Integer.parseInt(profileId));
    }

    private void renderView(User user) {
        String desc = "@" + user.username + "\n"
                + user.email + "\n" + user.phone;
        if (user.interests.size() > 0) desc += "\n\n" + "Interested in:";

        final String fullname = user.firstname + " " + user.lastname;
        ((TextView) mainView.findViewById(R.id.fullname)).setText(fullname);
        ((TextView) mainView.findViewById(R.id.description)).setText(desc);

        mainView.findViewById(R.id.btn_view_liked_trips).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ProfileLikeTripsActivity.userId = isSelfProfile() ? SettingsManager.currentUserId + "" : profileId;
                ProfileLikeTripsActivity.userName = fullname;
                Intent intent = new Intent(getContext(), ProfileLikeTripsActivity.class);
                getActivity().startActivityForResult(intent, Constants.REQUEST_CODE_NEW_TRIP);
            }
        });

        if (user.picture_url != null) {
            String trip_pic_url = user.picture_url;
            Picasso.get().load(trip_pic_url).into((ImageView) mainView.findViewById(R.id.avatar));
        }

        interestChipGroup.removeAllViews();
        for (Interest interest : user.interests) {
            interestChipGroup.addView(getInterestChip(interest));
        }
    }

    private Chip getInterestChip(Interest interest) {
        final Chip chip = new Chip(getContext());
        int paddingDp = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, 2,
                getResources().getDisplayMetrics()
        );
        chip.setPadding(paddingDp, paddingDp, paddingDp, paddingDp);
        chip.setText(interest.tag);
        /*chip.setOnCloseIconClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("chip", "clicked");
            }
        });*/
        chip.setClickable(false);
        //chip.setCloseIconVisible(true);
        return chip;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentProfileListener) {
            mListener = (OnFragmentProfileListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (shouldReload) {
            updateProfile();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            photoUploader.onActivityResultHandler(getActivity(), requestCode, resultCode, data, new AsyncResponse<JsonObject>() {
                @Override
                public void run() {
                    updateProfile();
                }
            });
        } catch (IOException e) {
            Log.e("error", "error " + e.toString());
            e.printStackTrace();
        }

        if (requestCode == Constants.REQUEST_CODE_NEW_TRIP && resultCode == RESULT_OK) {
            updateProfile();
        }
    }

    private void askLogout() {
        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case DialogInterface.BUTTON_POSITIVE:
                        settings.clearAuth();
                        Utils.clearAndOpenActivity(getActivity(), WelcomeActivity.class);
                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        dialog.dismiss();
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("Do you really want to logout?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();
    }

    public boolean isSelfProfile() {
        return profileId.equals("me") || Integer.parseInt(profileId) == SettingsManager.currentUserId;
    }

    public interface OnFragmentProfileListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
